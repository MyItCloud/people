package org.itstep.qa.people;

import org.kohsuke.randname.RandomNameGenerator;

import java.util.Arrays;
import java.util.Random;

public class Runner {
    public static void main(String[] args) {
        People[] one = iPeople(15);
        System.out.println(Arrays.toString(one));
    }


    private static People[] iPeople(int count) {
        RandomNameGenerator rnd = new RandomNameGenerator(0);
        People[] one = new People[count];
        for (int i = 0; i < one.length; i++) {
            one[i] = new People(
                    rnd.next(),
                    1900);
        }
        return one;
    }
}



