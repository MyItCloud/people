package org.itstep.qa.people;

import java.util.Date;
import java.util.Scanner;

public class People {
    private String name;
    private int dateBirth;

    public People(String name, int dateBirth) {
        this.name = name;
        this.dateBirth = dateBirth;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "People{" +
                "name='" + name + '\'' +
                ", dateBirth=" + dateBirth +
                '}';
    }
}
